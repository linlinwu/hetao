/*
 * hetao - High Performance Web Server
 * author	: calvin
 * email	: calvinwilliams@163.com
 *
 * Licensed under the LGPL v2.1, see the file LICENSE in base directory.
 */

#ifndef _H_HETAO_REST_
#define _H_HETAO_REST_

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <errno.h>

#include "hetao_socgi.h"

/*
 * error code
 **/

#define REST_ERROR_URI_FIRST_CHARACTER			211
#define REST_ERROR_TOO_MANY_HTTP_URI_PATHS		212
#define REST_ERROR_URI_FIRST_CHARACTER_IN_CONFIG	213
#define REST_ERROR_TOO_MANY_HTTP_URI_PATHS_IN_CONFIG	214
#define REST_ERROR_RESTSERVICE_ENTRY_RETURN		215
#define REST_ERROR_HTTP_DOMAIN				216
#define REST_ERROR_PLACEHOLDER_LACK_OF_CLOSE		221
#define REST_ERROR_BUFFER_OVERFLOW			222
#define REST_ERROR_NO_SERVICE_IN_CONFIG			223
#define REST_ERROR_GET_RESTSERVICECONTROLER		231
#define REST_ERROR_SOMETHING				299

#define REST_FATAL_CREATE_RESTSERVICECONTROLER		-211
#define REST_FATAL_ENV_VAR_NOT_FOUND			-221
#define REST_FATAL_SOMETHING				-299

/*
 * fasterhttp extension
 **/

#ifndef HTTP_METHOD_GET
#define HTTP_METHOD_GET					"GET"
#define HTTP_METHOD_POST				"POST"
#define HTTP_METHOD_HEAD				"HEAD"
#define HTTP_METHOD_TRACE				"TRACE"
#define HTTP_METHOD_OPTIONS				"OPTIONS"
#define HTTP_METHOD_PUT					"PUT"
#define HTTP_METHOD_DELETE				"DELETE"
#endif

/*
 * rest service context
 **/

struct RestServiceContext ;

#define RESTSERVICEENTRY		_WINDLL_FUNC funcRestServiceEntry

typedef int funcRestServiceEntry( struct RestServiceContext *ctx );

_WINDLL_FUNC struct HttpApplicationContext *RESTGetHttpApplication( struct RestServiceContext *ctx );

_WINDLL_FUNC char *RESTGetHttpMethodPtr( struct RestServiceContext *ctx , int *p_http_method_len );
_WINDLL_FUNC char *RESTGetHttpUriPtr( struct RestServiceContext *ctx , int *p_http_uri_len );

_WINDLL_FUNC int RESTGetHttpUriPathsCount( struct RestServiceContext *ctx );
_WINDLL_FUNC char *RESTGetHttpUriPathPtr( struct RestServiceContext *ctx , int index , int *p_path_len );
_WINDLL_FUNC int RESTGetHttpUriQueriesCount( struct RestServiceContext *ctx );
_WINDLL_FUNC char *RESTGetHttpUriQueryKeyPtr( struct RestServiceContext *ctx , int index , int *p_key_len );
_WINDLL_FUNC char *RESTGetHttpUriQueryValuePtr( struct RestServiceContext *ctx , int index , int *p_value_len );

_WINDLL_FUNC char *RESTGetHttpRequestBodyPtr( struct RestServiceContext *ctx , int *p_body_len );

_WINDLL_FUNC int RESTFormatHttpResponse( struct RestServiceContext *ctx , char *http_response_body , int http_response_body_len , char *http_header_format , ... );

/*
 * rest service controler
 **/

struct HttpEnv ;

struct RestServiceConfig
{
	char			http_method[ 7 + 1 ] ;
	char			http_uri_paths_match[ 255 + 1 ] ;
	funcRestServiceEntry	*pfuncRestServiceEntry ;
} ;

_WINDLL_FUNC struct RestServiceControler *RESTCreateRestServiceControler( struct RestServiceConfig *config_array );
_WINDLL_FUNC int RESTDispatchRestServiceControler( struct RestServiceControler *ctl , struct HttpApplicationContext *ctx );
_WINDLL_FUNC void RESTDestroyRestServiceControler( struct RestServiceControler *ctl );

#endif

