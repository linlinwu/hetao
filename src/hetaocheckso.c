/*
 * hetao - High Performance Web Server
 * author	: calvin
 * email	: calvinwilliams@163.com
 *
 * Licensed under the LGPL v2.1, see the file LICENSE in base directory.
 */

#include "hetao_in.h"

static void usage()
{
	printf( "hetaocheckso v%s build %s %s\n" , __HETAO_VERSION , __DATE__ , __TIME__ );
	printf( "USAGE : hetaocheckso *.socgi [-r]\n" );
	return;
}

int main( int argc , char *argv[] )
{
	char				*socgi_pathfilename = NULL ;
	int				unlink_on_error_flag ;
	
#if ( defined __linux ) || ( defined __unix )
	void				*so_handle ;
#elif ( defined _WIN32 )
	HINSTANCE			so_handle ;
#endif
	
	if( argc >= 1 + 1 )
	{
		socgi_pathfilename = argv[1] ;
		unlink_on_error_flag = 0 ;
		if( argc == 1 + 2 && STRCMP( argv[2] , == , "-r" ) )
			unlink_on_error_flag = 1 ;
		
		/* 设置缺省主日志 */
		SetLogFile( "" );
		SetLogLevel( LOGLEVEL_ERROR );
		SetLogPidCache();
		SetLogTidCache();
		UpdateLogDateTimeCacheFirst();
		
#if ( defined __linux ) || ( defined __unix )
		so_handle = dlopen( socgi_pathfilename , RTLD_NOW | RTLD_GLOBAL ) ;
		if( so_handle == NULL )
		{
			printf( "dlopen[%s] failed , errno[%d] dlerror[%s]\n" , socgi_pathfilename , errno , dlerror() );
			if( unlink_on_error_flag == 1 )
				unlink( socgi_pathfilename );
			return 1;
		}
#elif ( defined _WIN32 )
		so_handle = LoadLibrary( socgi_pathfilename ) ;
		if( so_handle == NULL )
		{
			printf( "dlopen[%s] failed , errno[%d]\n" , socgi_pathfilename , errno );
#if ( defined __linux ) || ( defined __unix )
			if( unlink_on_error_flag == 1 )
				unlink( socgi_pathfilename );
#elif ( defined _WIN32 )
			if( unlink_on_error_flag == 1 )
				DeleteFile( socgi_pathfilename );
#endif
			return 1;
		}
#endif		
		
#if ( defined __linux ) || ( defined __unix )
				dlclose( so_handle );
#elif ( defined _WIN32 )
				FreeLibrary( so_handle );
#endif
		
		printf( "OK\n" );
		
		return 0;
	}
	else
	{
		usage();
		exit(9);
	}
}

