/*
 * hetao - High Performance Web Server
 * author	: calvin
 * email	: calvinwilliams@163.com
 *
 * Licensed under the LGPL v2.1, see the file LICENSE in base directory.
 */

#include "hetao_in.h"

struct ForwardServer *CreateForwardServerListNode( char *ip , int port )
{
	struct ForwardServer	*p_forward_server = NULL ;
	
	if( ip[0] == '\0' || port <= 0 )
	{
		ErrorLog( __FILE__ , __LINE__ , "forward server ip invalid , ip[%s] port[%d]" , ip , port );
		return NULL;
	}
	
	p_forward_server = (struct ForwardServer *)malloc( sizeof(struct ForwardServer) ) ;
	if( p_forward_server == NULL )
	{
		ErrorLog( __FILE__ , __LINE__ , "malloc failed[%d] , errno[%d]" , ERRNO );
		return NULL;
	}
	memset( p_forward_server , 0x00 , sizeof(struct ForwardServer) );
	strncpy( p_forward_server->netaddr.ip , ip , sizeof(p_forward_server->netaddr.ip)-1 );
	p_forward_server->netaddr.port = port ;
	SETNETADDRESS( p_forward_server->netaddr )
	
	return p_forward_server;
}

int InitSoCgi( struct HetaoEnv *p_env , struct SoCgi *p_socgi , char *socgi_type , char *socgi_config_pathfilename , char *socgi_bin_pathfilename )
{
	int		nret = 0 ;
	
	strncpy( p_socgi->socgi_type , socgi_type , sizeof(p_socgi->socgi_type)-1 );
	p_socgi->socgi_type_len = strlen(p_socgi->socgi_type) ;
	strncpy( p_socgi->socgi_config_pathfilename , socgi_config_pathfilename , sizeof(p_socgi->socgi_config_pathfilename)-1 );
	strncpy( p_socgi->socgi_bin_pathfilename , socgi_bin_pathfilename , sizeof(p_socgi->socgi_bin_pathfilename)-1 );
	
	memset( & (p_socgi->http_application_context) , 0x00 , sizeof(struct HttpApplicationContext) );
	p_socgi->http_application_context.p_hetao_env = p_env ;
	p_socgi->http_application_context.p_http_session = NULL ;
	p_socgi->http_application_context.user_data = NULL ;
	
#if ( defined __linux ) || ( defined __unix )
	DebugLog( __FILE__ , __LINE__ , "dlopen[%s] ..." , p_socgi->socgi_bin_pathfilename );
	p_socgi->so_handle = dlopen( p_socgi->socgi_bin_pathfilename , RTLD_NOW | RTLD_GLOBAL ) ;
	if( p_socgi->so_handle == NULL )
	{
		ErrorLog( __FILE__ , __LINE__ , "dlopen[%s] failed , errno[%d] dlerror[%s]" , p_socgi->socgi_bin_pathfilename , errno , dlerror() );
		return -1;
	}
	else
	{
		InfoLog( __FILE__ , __LINE__ , "dlopen[%s] ok" , p_socgi->socgi_bin_pathfilename );
	}
	
	p_socgi->pfuncInitHttpApplication = (funcInitHttpApplication *)dlsym( p_socgi->so_handle , "InitHttpApplication" ) ;
	p_socgi->pfuncRedirectHttpDomain = (funcInitHttpApplication *)dlsym( p_socgi->so_handle , "RedirectHttpDomain" ) ;
	p_socgi->pfuncRewriteHttpUri = (funcInitHttpApplication *)dlsym( p_socgi->so_handle , "RewriteHttpUri" ) ;
	p_socgi->pfuncBeforeProcessHttpResource = (funcInitHttpApplication *)dlsym( p_socgi->so_handle , "BeforeProcessHttpResource" ) ;
	p_socgi->pfuncProcessHttpResource = (funcInitHttpApplication *)dlsym( p_socgi->so_handle , "ProcessHttpResource" ) ;
	p_socgi->pfuncSelectForwardServer = (funcInitHttpApplication *)dlsym( p_socgi->so_handle , "SelectForwardServer" ) ;
	p_socgi->pfuncCallHttpApplication = (funcCallHttpApplication *)dlsym( p_socgi->so_handle , "CallHttpApplication" ) ;
	p_socgi->pfuncGetHttpResource = (funcCleanHttpApplication *)dlsym( p_socgi->so_handle , "GetHttpResource" ) ;
	p_socgi->pfuncAfterProcessHttpResource = (funcCleanHttpApplication *)dlsym( p_socgi->so_handle , "AfterProcessHttpResource" ) ;
	p_socgi->pfuncCleanHttpApplication = (funcCleanHttpApplication *)dlsym( p_socgi->so_handle , "CleanHttpApplication" ) ;
#elif ( defined _WIN32 )
	DebugLog( __FILE__ , __LINE__ , "LoadLibrary[%s] ..." , p_socgi->socgi_bin_pathfilename );
	p_socgi->so_handle = LoadLibrary( TEXT(p_socgi->socgi_bin_pathfilename) ) ;
	if( p_socgi->so_handle == NULL )
	{
		ErrorLog( __FILE__ , __LINE__ , "LoadLibrary[%s] failed , errno[%d]" , p_socgi->socgi_bin_pathfilename , errno );
		return -1;
	}
	else
	{
		InfoLog( __FILE__ , __LINE__ , "LoadLibrary[%s] ok" , p_socgi->socgi_bin_pathfilename );
	}

	p_socgi->pfuncInitHttpApplication = (funcInitHttpApplication *)GetProcAddress( p_socgi->so_handle , "InitHttpApplication" ) ;
	p_socgi->pfuncRedirectHttpDomain = (funcInitHttpApplication *)GetProcAddress( p_socgi->so_handle , "RedirectHttpDomain" ) ;
	p_socgi->pfuncRewriteHttpUri = (funcInitHttpApplication *)GetProcAddress( p_socgi->so_handle , "RewriteHttpUri" ) ;
	p_socgi->pfuncBeforeProcessHttpResource = (funcInitHttpApplication *)GetProcAddress( p_socgi->so_handle , "BeforeProcessHttpResource" ) ;
	p_socgi->pfuncProcessHttpResource = (funcInitHttpApplication *)GetProcAddress( p_socgi->so_handle , "ProcessHttpResource" ) ;
	p_socgi->pfuncSelectForwardServer = (funcInitHttpApplication *)GetProcAddress( p_socgi->so_handle , "SelectForwardServer" ) ;
	p_socgi->pfuncCallHttpApplication = (funcCallHttpApplication *)GetProcAddress( p_socgi->so_handle , "CallHttpApplication" ) ;
	p_socgi->pfuncGetHttpResource = (funcCleanHttpApplication *)GetProcAddress( p_socgi->so_handle , "GetHttpResource" ) ;
	p_socgi->pfuncAfterProcessHttpResource = (funcCleanHttpApplication *)GetProcAddress( p_socgi->so_handle , "AfterProcessHttpResource" ) ;
	p_socgi->pfuncCleanHttpApplication = (funcCleanHttpApplication *)GetProcAddress( p_socgi->so_handle , "CleanHttpApplication" ) ;
#endif
	
	DebugLog( __FILE__ , __LINE__ , "dlsym pfuncInitHttpApplication[%p]" , p_socgi->pfuncInitHttpApplication );
	DebugLog( __FILE__ , __LINE__ , "dlsym pfuncRedirectHttpDomain[%p]" , p_socgi->pfuncRedirectHttpDomain );
	DebugLog( __FILE__ , __LINE__ , "dlsym pfuncRewriteHttpUri[%p]" , p_socgi->pfuncRewriteHttpUri );
	DebugLog( __FILE__ , __LINE__ , "dlsym pfuncBeforeProcessHttpResource[%p]" , p_socgi->pfuncBeforeProcessHttpResource );
	DebugLog( __FILE__ , __LINE__ , "dlsym pfuncProcessHttpResource[%p]" , p_socgi->pfuncProcessHttpResource );
	DebugLog( __FILE__ , __LINE__ , "dlsym pfuncSelectForwardServer[%p]" , p_socgi->pfuncSelectForwardServer );
	DebugLog( __FILE__ , __LINE__ , "dlsym pfuncCallHttpApplication[%p]" , p_socgi->pfuncCallHttpApplication );
	DebugLog( __FILE__ , __LINE__ , "dlsym pfuncGetHttpResource[%p]" , p_socgi->pfuncGetHttpResource );
	DebugLog( __FILE__ , __LINE__ , "dlsym pfuncAfterProcessHttpResource[%p]" , p_socgi->pfuncAfterProcessHttpResource );
	DebugLog( __FILE__ , __LINE__ , "dlsym pfuncCleanHttpApplication[%p]" , p_socgi->pfuncCleanHttpApplication );
	
	if( p_socgi->pfuncInitHttpApplication )
	{
		InfoLog( __FILE__ , __LINE__ , "call pfuncInitHttpApplication[%p] ..." , p_socgi->pfuncInitHttpApplication );
		nret = p_socgi->pfuncInitHttpApplication( & (p_socgi->http_application_context) ) ;
		if( nret < 0 )
		{
			FatalLog( __FILE__ , __LINE__ , "call pfuncInitHttpApplication[%p] failed[%d]" , p_socgi->pfuncInitHttpApplication  , nret );
			exit(1);
		}
		else if( nret != HTTP_OK )
		{
			ErrorLog( __FILE__ , __LINE__ , "call pfuncInitHttpApplication[%p] failed[%d]" , p_socgi->pfuncInitHttpApplication , nret );
			return -1;
		}
		else
		{
			InfoLog( __FILE__ , __LINE__ , "call pfuncInitHttpApplication[%p] ok" , p_socgi->pfuncInitHttpApplication );
		}
	}
	
	return 0;
}

void CleanSoCgi( struct SoCgi *p_socgi )
{
	int		nret = 0 ;
	
	if( p_socgi->socgi_bin_pathfilename[0] )
	{
		if( p_socgi->pfuncCleanHttpApplication )
		{
			DebugLog( __FILE__ , __LINE__ , "call pfuncCleanHttpApplication[%p] ..." , p_socgi->pfuncCleanHttpApplication );
			nret = p_socgi->pfuncCleanHttpApplication( & (p_socgi->http_application_context) ) ;
			if( nret < 0 )
			{
				FatalLog( __FILE__ , __LINE__ , "call pfuncCleanHttpApplication[%p] failed[%d]" , p_socgi->pfuncCleanHttpApplication , nret );
				exit(1);
			}
			else if( nret != HTTP_OK )
			{
				ErrorLog( __FILE__ , __LINE__ , "call pfuncCleanHttpApplicationi[%p] failed[%d]" , p_socgi->pfuncCleanHttpApplication , nret );
			}
			else
			{
				InfoLog( __FILE__ , __LINE__ , "call pfuncCleanHttpApplication[%p] ok" , p_socgi->pfuncCleanHttpApplication );
			}
		}
		
#if ( defined __linux ) || ( defined __unix )
		dlclose( p_socgi->so_handle );
#elif ( defined _WIN32 )
		FreeLibrary( p_socgi->so_handle );
#endif
	}
	
	return;
}

int InitVirtualHostHash( struct VirtualHosts *p_virtual_host_hash , int count )
{
	int			i ;
	struct hlist_head	*p_hlist_head = NULL ;
	
	p_virtual_host_hash->virtual_host_hashsize = count * 2 ;
	p_virtual_host_hash->virtual_host_hash = (struct hlist_head *)malloc( sizeof(struct hlist_head) * p_virtual_host_hash->virtual_host_hashsize ) ;
	if( p_virtual_host_hash->virtual_host_hash == NULL )
	{
		ErrorLog( __FILE__ , __LINE__ , "malloc failed , errno[%d]" , ERRNO );
		return -1;
	}
	memset( p_virtual_host_hash->virtual_host_hash , 0x00 , sizeof(struct hlist_head) * p_virtual_host_hash->virtual_host_hashsize );
	
	for( i = 0 , p_hlist_head = p_virtual_host_hash->virtual_host_hash ; i < p_virtual_host_hash->virtual_host_hashsize ; i++ , p_hlist_head++ )
	{
		INIT_HLIST_HEAD( p_hlist_head );
	}
	
	p_virtual_host_hash->virtual_host_count = 0 ;
	
	return 0;
}

struct UriLocation *CreateUriLocation( char *location )
{
	struct UriLocation	*p_uri_location = NULL ;
	const char		*error_desc = NULL ;
	int			error_offset ;
	
	p_uri_location = (struct UriLocation *)malloc( sizeof(struct UriLocation) ) ;
	if( p_uri_location == NULL )
	{
		ErrorLog( __FILE__ , __LINE__ , "malloc UriLocation failed , errno[%d]" , ERRNO );
		return NULL;
	}
	memset( p_uri_location , 0x00 , sizeof(struct UriLocation) );
	
	strncpy( p_uri_location->location , location , sizeof(p_uri_location->location)-1 );
	
	p_uri_location->location_re = pcre_compile( p_uri_location->location , 0 , & error_desc , & error_offset , NULL ) ;
	if( p_uri_location->location_re == NULL )
	{
		ErrorLog( __FILE__ , __LINE__ , "pcre_compile[%s] failed[%s][%d]" , p_uri_location->location , error_desc , error_offset );
		return NULL;
	}
	
	return p_uri_location;
}
	
struct VirtualHost *CreateVirtualHostHashNode( struct HetaoEnv *p_env , hetao_conf *p_conf , int l , int w , char *domain , char *wwwroot , char *index , char *access_log )
{
	int			d , r , o ;
	struct VirtualHost	*p_virtual_host = NULL ;
	struct RedirectDomain	*p_redirect_domain = NULL ;
	struct RewriteUriNode	*p_rewrite_uri = NULL ;
	struct UriLocation	*p_uri_location = NULL ;
	const char		*error_desc = NULL ;
	int			error_offset ;
	
	int			nret = 0 ;
	
	/* 创建虚拟主机结构 */
	p_virtual_host = (struct VirtualHost *)malloc( sizeof(struct VirtualHost) ) ;
	if( p_virtual_host == NULL )
	{
		ErrorLog( __FILE__ , __LINE__ , "malloc failed , errno[%d]" , ERRNO );
		return NULL;
	}
	memset( p_virtual_host , 0x00 , sizeof(struct VirtualHost) );
	
	p_virtual_host->type = DATASESSION_TYPE_HTMLCACHE ;
	
	strncpy( p_virtual_host->domain , p_conf->listen[l].website[w].domain , sizeof(p_virtual_host->domain)-1 );
	p_virtual_host->domain_len = strlen(p_virtual_host->domain) ;
	strncpy( p_virtual_host->wwwroot , p_conf->listen[l].website[w].wwwroot , sizeof(p_virtual_host->wwwroot)-1 );
	strncpy( p_virtual_host->index , p_conf->listen[l].website[w].index , sizeof(p_virtual_host->index)-1 );
	strncpy( p_virtual_host->access_log , p_conf->listen[l].website[w].access_log , sizeof(p_virtual_host->access_log)-1 );
	if(  p_virtual_host->access_log[0] )
	{
		p_virtual_host->access_log_fd = OPEN( p_virtual_host->access_log , O_CREAT_WRONLY_APPEND ) ;
		if( p_virtual_host->access_log_fd == -1 )
		{
			ErrorLog( __FILE__ , __LINE__ , "open access log[%s] failed , errno[%d]" , p_virtual_host->access_log , ERRNO );
			return NULL;
		}
		SetHttpCloseExec( p_virtual_host->access_log_fd );
	}
	else
	{
		p_virtual_host->access_log_fd = -1 ;
	}
	
	/* 注册所有域名重定向哈希表 */
	nret = InitRedirectDomainHash( & (p_virtual_host->uri_location_default.redirect_domains) , p_conf->listen[l].website[w]._redirect_count ) ;
	if( nret )
	{
		ErrorLog( __FILE__ , __LINE__ , "InitRedirectDomainHash failed[%d]" , nret );
		return NULL;
	}
	
	for( d = 0 ; d < p_conf->listen[l].website[w]._redirect_count ; d++ )
	{
		p_redirect_domain = CreateRedirectDomainHashNode( p_conf->listen[l].website[w].redirect[d].domain , p_conf->listen[l].website[w].redirect[d].new_domain ) ;
		if( p_redirect_domain == NULL )
		{
			ErrorLog( __FILE__ , __LINE__ , "CreateRedirectDomainHashNode failed" );
			return NULL;
		}
		else
		{
			InfoLog( __FILE__ , __LINE__ , "CreateRedirectDomainHashNode ok" );
		}
		
		nret = PushRedirectDomainHashNode( & (p_virtual_host->uri_location_default.redirect_domains) , p_redirect_domain ) ;
		if( nret )
		{
			ErrorLog( __FILE__ , __LINE__ , "PushRedirectDomainHashNode[%s][%s] failed[%d] , errno[%d]" , p_redirect_domain->domain , p_redirect_domain->new_domain , nret , ERRNO );
			return NULL;
		}
		else
		{
			DebugLog( __FILE__ , __LINE__ , "add redirect[%s][%s]" , p_redirect_domain->domain , p_redirect_domain->new_domain );
		}
		
		if( p_redirect_domain->domain[0] == '\0' && p_virtual_host->uri_location_default.redirect_domains.p_redirect_domain_default == NULL )
			p_virtual_host->uri_location_default.redirect_domains.p_redirect_domain_default = p_redirect_domain ;
	}
	
	/* 注册所有重写URI链表 */
	memset( & (p_virtual_host->uri_location_default.rewrite_uris) , 0x00 , sizeof(struct RewriteUriNode) );
	INIT_LIST_HEAD( & (p_virtual_host->uri_location_default.rewrite_uris.rewrite_uri_list.rewrite_uri_node) );
	
	if( p_conf->listen[l].website[w]._rewrite_count > 0 && p_env->new_uri_re == NULL )
	{
		if( p_env->new_uri_re == NULL )
		{
			p_env->new_uri_re = pcre_compile( TEMPLATE_PATTERN , 0 , & error_desc , & error_offset , NULL ) ;
			if( p_env->new_uri_re == NULL )
			{
				ErrorLog( __FILE__ , __LINE__ , "pcre_compile[%s] failed[%s][%d]" , TEMPLATE_PATTERN , error_desc , error_offset );
				return NULL;
			}
			DebugLog( __FILE__ , __LINE__ , "create new_uri pattern[%s]" , TEMPLATE_PATTERN );
		}
	}	
	
	for( r = 0 ; r < p_conf->listen[l].website[w]._rewrite_count ; r++ )
	{
		p_rewrite_uri = CreateRewriteUriListNode( p_conf->listen[l].website[w].rewrite[r].pattern , p_conf->listen[l].website[w].rewrite[r].new_uri ) ;
		if( p_rewrite_uri == NULL )
		{
			ErrorLog( __FILE__ , __LINE__ , "CreateRewriteUriListNode failed" );
			return NULL;
		}
		else
		{
			InfoLog( __FILE__ , __LINE__ , "CreateRewriteUriListNode failed" );
		}

		list_add_tail( & (p_rewrite_uri->rewrite_uri_node) , & (p_virtual_host->uri_location_default.rewrite_uris.rewrite_uri_list.rewrite_uri_node) );
		DebugLog( __FILE__ , __LINE__ , "add rewrite[%s][%s]" , p_rewrite_uri->pattern , p_rewrite_uri->new_uri );
	}
	
	/* 注册所有反向代理链表 */
	strncpy( p_virtual_host->uri_location_default.forward_servers.forward_type , p_conf->listen[l].website[w].forward.forward_type , sizeof(p_virtual_host->uri_location_default.forward_servers.forward_type)-1 );
	p_virtual_host->uri_location_default.forward_servers.forward_type_len = strlen(p_virtual_host->uri_location_default.forward_servers.forward_type) ;
	strncpy( p_virtual_host->uri_location_default.forward_servers.forward_rule , p_conf->listen[l].website[w].forward.forward_rule , sizeof(p_virtual_host->uri_location_default.forward_servers.forward_rule)-1 );
	
	if( p_conf->listen[l].website[w].forward.ssl.certificate_file[0] )
	{
		if( p_env->init_ssl_env_flag == 0 )
		{
			SSL_library_init();
			OpenSSL_add_ssl_algorithms();
			SSL_load_error_strings();
			p_env->init_ssl_env_flag = 1 ;
		}
		
		p_virtual_host->uri_location_default.forward_servers.forward_ssl_ctx = SSL_CTX_new( SSLv23_method() ) ;
		if( p_virtual_host->uri_location_default.forward_servers.forward_ssl_ctx == NULL )
		{
			ErrorLog( __FILE__ , __LINE__ , "SSL_CTX_new failed , errno[%d]" , ERRNO );
			return NULL;
		}
		
		nret = SSL_CTX_use_certificate_file( p_virtual_host->uri_location_default.forward_servers.forward_ssl_ctx , p_conf->listen[l].website[w].forward.ssl.certificate_file , SSL_FILETYPE_PEM ) ;
		if( nret <= 0 )
		{
			ErrorLog( __FILE__ , __LINE__ , "SSL_CTX_use_certificate_file failed , errno[%d]" , ERRNO );
			return NULL;
		}
		else
		{
			DebugLog( __FILE__ , __LINE__ , "SSL_CTX_use_certificate_file[%s] ok" , p_conf->listen[l].website[w].forward.ssl.certificate_file );
		}
	}
	
	INIT_LIST_HEAD( & (p_virtual_host->uri_location_default.forward_servers.roundrobin_list.roundrobin_node) );
	
	/* 注册反向代理下游服务器 */
	if( p_virtual_host->uri_location_default.forward_servers.forward_rule[0] && p_conf->listen[l].website[w].forward._forward_server_count > 0 )
	{
		struct ForwardServer	*p_forward_server = NULL ;
		
		for( d = 0 ; d < p_conf->listen[l].website[w].forward._forward_server_count ; d++ )
		{
			p_forward_server = CreateForwardServerListNode( p_conf->listen[l].website[w].forward.forward_server[d].ip , p_conf->listen[l].website[w].forward.forward_server[d].port ) ;
			if( p_forward_server == NULL )
			{
				ErrorLog( __FILE__ , __LINE__ , "CreateForwardServerListNode failed" );
				return NULL;
			}
			else
			{
				InfoLog( __FILE__ , __LINE__ , "CreateForwardServerListNode ok" );
			}
			
			list_add_tail( & (p_forward_server->roundrobin_node) , & (p_virtual_host->uri_location_default.forward_servers.roundrobin_list.roundrobin_node) );
			AddLeastConnectionCountTreeNode( & (p_virtual_host->uri_location_default.forward_servers) , p_forward_server );
		}
	}
	
	/* 注册SOCGI应用 */
	if( p_conf->listen[l].website[w].socgi.socgi_bin_pathfilename[0] )
	{
		nret = InitSoCgi( p_env , & (p_virtual_host->uri_location_default.socgi) , p_conf->listen[l].website[w].socgi.socgi_type , p_conf->listen[l].website[w].socgi.socgi_config_pathfilename , p_conf->listen[l].website[w].socgi.socgi_bin_pathfilename ) ;
		if( nret )
		{
			ErrorLog( __FILE__ , __LINE__ , "InitSoCgi failed" );
			return NULL;
		}
		else
		{
			InfoLog( __FILE__ , __LINE__ , "InitSoCgi ok" );
		}
	}
	
	/* 注册所有Location */
	INIT_LIST_HEAD( & (p_virtual_host->uri_locations.uri_location_node) );
	
	for( o = 0 ; o < p_conf->listen[l].website[w]._location_count ; o++ )
	{
		p_uri_location = CreateUriLocation( p_conf->listen[l].website[w].location[o].location ) ;
		if( p_uri_location == NULL )
		{
			ErrorLog( __FILE__ , __LINE__ , "CreateUriLocation failed , location[%s]" , p_conf->listen[l].website[w].location[o].location );
			return NULL;
		}
		else
		{
			InfoLog( __FILE__ , __LINE__ , "CreateUriLocation ok , location[%s]" , p_conf->listen[l].website[w].location[o].location );
		}
		
		/* 注册所有域名重定向哈希表 */
		nret = InitRedirectDomainHash( & (p_uri_location->redirect_domains) , p_conf->listen[l].website[w].location[o]._redirect_count ) ;
		if( nret )
		{
			ErrorLog( __FILE__ , __LINE__ , "InitRedirectDomainHash failed[%d]" , nret );
			return NULL;
		}
		
		for( d = 0 ; d < p_conf->listen[l].website[w].location[o]._redirect_count ; d++ )
		{
			p_redirect_domain = CreateRedirectDomainHashNode( p_conf->listen[l].website[w].location[o].redirect[d].domain , p_conf->listen[l].website[w].location[o].redirect[d].new_domain ) ;
			if( p_redirect_domain == NULL )
			{
				ErrorLog( __FILE__ , __LINE__ , "CreateRedirectDomainHashNode failed" );
				return NULL;
			}
			else
			{
				InfoLog( __FILE__ , __LINE__ , "CreateRedirectDomainHashNode ok" );
			}
			
			nret = PushRedirectDomainHashNode( & (p_uri_location->redirect_domains) , p_redirect_domain ) ;
			if( nret )
			{
				ErrorLog( __FILE__ , __LINE__ , "PushRedirectDomainHashNode[%s][%s] failed[%d] , errno[%d]" , p_redirect_domain->domain , p_redirect_domain->new_domain , nret , ERRNO );
				return NULL;
			}
			else
			{
				DebugLog( __FILE__ , __LINE__ , "add redirect[%s][%s]" , p_redirect_domain->domain , p_redirect_domain->new_domain );
			}
			
			if( p_redirect_domain->domain[0] == '\0' && p_uri_location->redirect_domains.p_redirect_domain_default == NULL )
				p_uri_location->redirect_domains.p_redirect_domain_default = p_redirect_domain ;
		}
		
		/* 注册所有重写URI链表 */
		memset( & (p_uri_location->rewrite_uris) , 0x00 , sizeof(struct RewriteUriNode) );
		INIT_LIST_HEAD( & (p_uri_location->rewrite_uris.rewrite_uri_list.rewrite_uri_node) );
		
		if( p_conf->listen[l].website[w].location[o]._rewrite_count > 0 && p_env->new_uri_re == NULL )
		{
			if( p_env->new_uri_re == NULL )
			{
				p_env->new_uri_re = pcre_compile( TEMPLATE_PATTERN , 0 , & error_desc , & error_offset , NULL ) ;
				if( p_env->new_uri_re == NULL )
				{
					ErrorLog( __FILE__ , __LINE__ , "pcre_compile[%s] failed[%s][%d]" , TEMPLATE_PATTERN , error_desc , error_offset );
					return NULL;
				}
				DebugLog( __FILE__ , __LINE__ , "create new_uri pattern[%s]" , TEMPLATE_PATTERN );
			}
		}	
		
		for( r = 0 ; r < p_conf->listen[l].website[w].location[o]._rewrite_count ; r++ )
		{
			p_rewrite_uri = CreateRewriteUriListNode( p_conf->listen[l].website[w].location[o].rewrite[r].pattern , p_conf->listen[l].website[w].location[o].rewrite[r].new_uri ) ;
			if( p_rewrite_uri == NULL )
			{
				ErrorLog( __FILE__ , __LINE__ , "CreateRewriteUriListNode failed" );
				return NULL;
			}
			else
			{
				InfoLog( __FILE__ , __LINE__ , "CreateRewriteUriListNode failed" );
			}

			list_add_tail( & (p_rewrite_uri->rewrite_uri_node) , & (p_uri_location->rewrite_uris.rewrite_uri_list.rewrite_uri_node) );
			DebugLog( __FILE__ , __LINE__ , "add rewrite[%s][%s]" , p_rewrite_uri->pattern , p_rewrite_uri->new_uri );
		}
		
		/* 注册所有反向代理链表 */
		strncpy( p_uri_location->forward_servers.forward_type , p_conf->listen[l].website[w].location[o].forward.forward_type , sizeof(p_uri_location->forward_servers.forward_type)-1 );
		p_uri_location->forward_servers.forward_type_len = strlen(p_uri_location->forward_servers.forward_type) ;
		strncpy( p_uri_location->forward_servers.forward_rule , p_conf->listen[l].website[w].location[o].forward.forward_rule , sizeof(p_uri_location->forward_servers.forward_rule)-1 );
		
		if( p_conf->listen[l].website[w].location[o].forward.ssl.certificate_file[0] )
		{
			if( p_env->init_ssl_env_flag == 0 )
			{
				SSL_library_init();
				OpenSSL_add_ssl_algorithms();
				SSL_load_error_strings();
				p_env->init_ssl_env_flag = 1 ;
			}
			
			p_uri_location->forward_servers.forward_ssl_ctx = SSL_CTX_new( SSLv23_method() ) ;
			if( p_uri_location->forward_servers.forward_ssl_ctx == NULL )
			{
				ErrorLog( __FILE__ , __LINE__ , "SSL_CTX_new failed , errno[%d]" , ERRNO );
				return NULL;
			}
			
			nret = SSL_CTX_use_certificate_file( p_uri_location->forward_servers.forward_ssl_ctx , p_conf->listen[l].website[w].location[o].forward.ssl.certificate_file , SSL_FILETYPE_PEM ) ;
			if( nret <= 0 )
			{
				ErrorLog( __FILE__ , __LINE__ , "SSL_CTX_use_certificate_file failed , errno[%d]" , ERRNO );
				return NULL;
			}
			else
			{
				DebugLog( __FILE__ , __LINE__ , "SSL_CTX_use_certificate_file[%s] ok" , p_conf->listen[l].website[w].location[o].forward.ssl.certificate_file );
			}
		}
		
		INIT_LIST_HEAD( & (p_uri_location->forward_servers.roundrobin_list.roundrobin_node) );
		
		/* 注册反向代理下游服务器 */
		if( p_uri_location->forward_servers.forward_rule[0] && p_conf->listen[l].website[w].location[o].forward._forward_server_count > 0 )
		{
			struct ForwardServer	*p_forward_server = NULL ;
			
			for( d = 0 ; d < p_conf->listen[l].website[w].location[o].forward._forward_server_count ; d++ )
			{
				p_forward_server = CreateForwardServerListNode( p_conf->listen[l].website[w].location[o].forward.forward_server[d].ip , p_conf->listen[l].website[w].location[o].forward.forward_server[d].port ) ;
				if( p_forward_server == NULL )
				{
					ErrorLog( __FILE__ , __LINE__ , "CreateForwardServerListNode failed" );
					return NULL;
				}
				else
				{
					InfoLog( __FILE__ , __LINE__ , "CreateForwardServerListNode ok" );
				}
				
				list_add_tail( & (p_forward_server->roundrobin_node) , & (p_uri_location->forward_servers.roundrobin_list.roundrobin_node) );
				AddLeastConnectionCountTreeNode( & (p_uri_location->forward_servers) , p_forward_server );
			}
		}
		
		/* 注册SOCGI应用 */
		if( p_conf->listen[l].website[w].location[o].socgi.socgi_bin_pathfilename[0] )
		{
			nret = InitSoCgi( p_env , & (p_uri_location->socgi) , p_conf->listen[l].website[w].location[o].socgi.socgi_type , p_conf->listen[l].website[w].location[o].socgi.socgi_config_pathfilename , p_conf->listen[l].website[w].location[o].socgi.socgi_bin_pathfilename ) ;
			if( nret )
			{
				ErrorLog( __FILE__ , __LINE__ , "InitSoCgi failed" );
				return NULL;
			}
			else
			{
				InfoLog( __FILE__ , __LINE__ , "InitSoCgi ok" );
			}
		}
		
		list_add_tail( & (p_uri_location->uri_location_node) , & (p_virtual_host->uri_locations.uri_location_node) );
	}
	
	return p_virtual_host;
}

void CleanVirtualHostHash( struct VirtualHosts *p_virtual_hosts )
{
	int			v ;
	struct hlist_head	*p_hlist_head = NULL ;
	struct hlist_node	*curr = NULL , *next = NULL ;
	struct VirtualHost	*p_virtual_host = NULL ;
	struct RewriteUriNode	*p_rewrite_uri = NULL ;
	struct RewriteUriNode	*p_next_rewrite_uri = NULL ;
	struct ForwardServer	*p_forward_server = NULL ;
	struct ForwardServer	*p_next_forward_server = NULL ;
	struct UriLocation	*p_uri_location = NULL ;
	struct UriLocation	*p_next_uri_location = NULL ;
	
	for( v = 0 , p_hlist_head = p_virtual_hosts->virtual_host_hash ; v < p_virtual_hosts->virtual_host_hashsize ; v++ , p_hlist_head++ )
	{
		hlist_for_each_safe( curr , next , p_hlist_head )
		{
			hlist_del( curr );
			
			p_virtual_host = container_of(curr,struct VirtualHost,virtual_host_node) ;
			
			CleanRedirectDomainHash( & (p_virtual_host->uri_location_default.redirect_domains) );
			
			list_for_each_entry_safe( p_rewrite_uri , p_next_rewrite_uri , & (p_virtual_host->uri_location_default.rewrite_uris.rewrite_uri_list.rewrite_uri_node) , struct RewriteUriNode , rewrite_uri_node )
			{
				free( p_rewrite_uri->pattern_re );
			}
			
			if( p_virtual_host->uri_location_default.forward_servers.forward_ssl_ctx )
			{
				SSL_CTX_free( p_virtual_host->uri_location_default.forward_servers.forward_ssl_ctx );
				p_virtual_host->uri_location_default.forward_servers.forward_ssl_ctx = NULL ;
			}
			list_for_each_entry_safe( p_forward_server , p_next_forward_server , & (p_virtual_host->uri_location_default.forward_servers.roundrobin_list.roundrobin_node) , struct ForwardServer , roundrobin_node )
			{
				free( p_forward_server );
			}
			
			CleanSoCgi( & (p_virtual_host->uri_location_default.socgi) );
			
			list_for_each_entry_safe( p_uri_location , p_next_uri_location , & (p_virtual_host->uri_locations.uri_location_node) , struct UriLocation , uri_location_node )
			{
				CleanRedirectDomainHash( & (p_uri_location->redirect_domains) );
				
				list_for_each_entry_safe( p_rewrite_uri , p_next_rewrite_uri , & (p_uri_location->rewrite_uris.rewrite_uri_list.rewrite_uri_node) , struct RewriteUriNode , rewrite_uri_node )
				{
					free( p_rewrite_uri->pattern_re );
				}
				
				if( p_uri_location->forward_servers.forward_ssl_ctx )
				{
					SSL_CTX_free( p_uri_location->forward_servers.forward_ssl_ctx );
					p_uri_location->forward_servers.forward_ssl_ctx = NULL ;
				}
				list_for_each_entry_safe( p_forward_server , p_next_forward_server , & (p_uri_location->forward_servers.roundrobin_list.roundrobin_node) , struct ForwardServer , roundrobin_node )
				{
					free( p_forward_server );
				}
				
				CleanSoCgi( & (p_uri_location->socgi) );
			}
			
#if ( defined _WIN32 )
			CloseHandle( p_virtual_host->directory_changes_handler );
#endif
			
			free( p_virtual_host );
		}
	}
	
	free( p_virtual_hosts->virtual_host_hash );
	p_virtual_hosts->virtual_host_hash = NULL ;
	
	return;
}

int PushVirtualHostHashNode( struct VirtualHosts *p_virtual_host_hash , struct VirtualHost *p_virtual_host )
{
	int			index ;
	struct hlist_head	*p_hlist_head = NULL ;
	struct VirtualHost	*p ;
	
	index = CalcHash(p_virtual_host->domain,p_virtual_host->domain_len) % (p_virtual_host_hash->virtual_host_hashsize) ;
	p_hlist_head = p_virtual_host_hash->virtual_host_hash + index ;
	hlist_for_each_entry( p , p_hlist_head , struct VirtualHost , virtual_host_node )
	{
		if( STRCMP( p->domain , == , p_virtual_host->domain ) )
			return 1;
	}
	hlist_add_head( & (p_virtual_host->virtual_host_node) , p_hlist_head );
	
	p_virtual_host_hash->virtual_host_count++;
	
	return 0;
}

struct VirtualHost *QueryVirtualHostHashNode( struct VirtualHosts *p_virtual_host_hash , char *domain , int domain_len )
{
	int			index ;
	struct hlist_head	*p_hlist_head = NULL ;
	struct VirtualHost	*p ;
	
	index = CalcHash(domain,domain_len) % (p_virtual_host_hash->virtual_host_hashsize) ;
	p_hlist_head = p_virtual_host_hash->virtual_host_hash + index ;
	hlist_for_each_entry( p , p_hlist_head , struct VirtualHost , virtual_host_node )
	{
		if( p->domain_len == domain_len && STRNCMP( p->domain , == , domain , domain_len ) )
			return p;
	}
	
	return NULL;
}

