#ifndef _H_REST_FULL_
#define _H_REST_FULL_

#include "hetao_rest.h"

#include "LOGC.h"

void TravelUriPathsAndQueries( struct RestServiceContext *ctx , char *buf , int buf_size , int *p_buf_len );

RESTSERVICEENTRY GET_;
RESTSERVICEENTRY GET_path1;
RESTSERVICEENTRY GET_path1_;
RESTSERVICEENTRY GET_path1_path2;
RESTSERVICEENTRY GET_path1_path2_;
RESTSERVICEENTRY GET_path1_path2_file;
RESTSERVICEENTRY GET_path1_n_file;

RESTSERVICEENTRY GET_path1_path2_file1__key1_value1;
RESTSERVICEENTRY GET_path1_path2_file2__key1_value1__key2_value2;
RESTSERVICEENTRY GET_path1_path2_file3__;
RESTSERVICEENTRY GET_path1_path2_file4__key1;
RESTSERVICEENTRY GET_path1_path2_file5__key1_;
RESTSERVICEENTRY GET_path1_path2_file6__key1__;
RESTSERVICEENTRY GET_path1_path2_file7__key1___;

RESTSERVICEENTRY POST_path1_file;

RESTSERVICEENTRY PUT_path1_file;

RESTSERVICEENTRY DELETE_path1_file;

#endif

